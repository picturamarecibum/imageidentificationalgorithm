"""
Trains a simple model with logistic regression on the MNIST dataset (digits from 0 to 9)

"""

from mnist import MNIST
import numpy as np
import time
from sklearn import linear_model as lm
from matplotlib import pyplot as plt

# Load in the MNIST dataset and reshape it into a 3d array
mndata = MNIST()
images, labels = mndata.load_training()
test_images, test_labels = mndata.load_testing()
train_dataset = np.array(images).reshape(len(images), 28, 28)
test_dataset = np.array(test_images).reshape(len(test_images), 28, 28)

samples, width, height = train_dataset.shape
testsamples, testwidth, testheight = test_dataset.shape


def train_model(sample_amount):
    """
    Reshape the train and test dataset into a 2d array and then apply logistic regression on the datasets.
    :param sample_amount:
    :return:
    """
    start = time.time()
    X = np.reshape(train_dataset, (samples, width * height))[0:sample_amount]
    testX = np.reshape(test_dataset, (testsamples, testwidth * testheight))
    Y = labels[0:sample_amount]

    model = lm.LogisticRegression()
    model.fit(X, Y)
    predictions = model.predict(testX)

    # Print sample prediction
    print("Predicts: {}".format(predictions[0:25]))
    print("Real: {}".format(test_labels[0:25]))

    error = np.equal(predictions, test_labels)
    print("{} training samples, accuracy: {}, took {:.2f} seconds.".format(sample_amount,
                                                                       float(sum(error)) / float(len(test_labels)),
                                                                       time.time() - start))
    return 0


# Train the model using X amount of training samples.
train_model(50)
# train_model(100)
# train_model(1000)
# train_model(5000)
# train_model(50000)

# Show a sample image
figure = plt.figure(figsize=(1, 5))
z = 0
for i in test_dataset[:25]:
    z += 1
    figure.add_subplot(1, 25, z)
    plt.imshow(i)

plt.show(block=True)
