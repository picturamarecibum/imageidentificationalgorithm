# Assesssing Ocean Health

For the 2015 Data Science Bowl, 150.000 images of sea plankton were made available in a contest to build the best classifier for the plankton.
In this repository several versions of classifiers can be found, using different techniques which are able to classify the plankton images. 
The following 3 versions are available:

- simple_plankton_network.py (which uses a recurrent neural network)
- conv_plankton_network.py (Uses a convolutional neural network in TensorFlow)
- tflearn_conv_plankton.py (Uses a convolutional neural network in TFLearn)


## Usage

* Run the version of choice e.g.  
``python3 tflearn_conv_plankton.py``

## Software 

The following libraries that are used in additional with Python3:

| Python3 packages:      | Version|
| ------------- |:-------------:|
| Multiprocessing | 0.70.5 |
| Numpy | 1.14.2 |
| Scikit-image | 0.13.1 |
| Tensorflow | 1.5.0 |
| Tflearn | 0.3.2 |
