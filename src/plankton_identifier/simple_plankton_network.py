import tensorflow as tf
import plankton_preprocess
import data_shuffle
import numpy as np

train_set, train_labels, test_set, test_labels = data_shuffle.add_files_to_process(
    "/commons/student/2017-2018/Thema11/Lin_Sylt/")

train_set = np.array(train_set)
test_set = np.array(test_set)
train_set = np.reshape(train_set, (train_set.shape[0], train_set.shape[1] * train_set.shape[2]))
test_set = np.reshape(test_set, (test_set.shape[0], test_set.shape[1] * test_set.shape[2]))

n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500
n_classes = 5
batch_size = 100

x = tf.placeholder('float', [None, 183184])
y = tf.placeholder('float')


def neural_network_model(data):
    hidden_1_layer = {'weights': tf.Variable(tf.random_normal([183184, n_nodes_hl1])),
                      'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))}

    hidden_2_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
                      'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))}

    hidden_3_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
                      'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))}

    output_layer = {'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_classes])),
                    'biases': tf.Variable(tf.random_normal([n_classes]))}

    hl1 = tf.add(tf.matmul(data, hidden_1_layer["weights"]), hidden_1_layer["biases"])
    hl2 = tf.nn.relu(tf.add(tf.matmul(hl1, hidden_2_layer["weights"]), hidden_2_layer["biases"]))
    hl3 = tf.nn.relu(tf.add(tf.matmul(hl2, hidden_3_layer["weights"]), hidden_3_layer["biases"]))

    return tf.add(tf.matmul(hl3, output_layer["weights"]), output_layer["biases"])


def train_neural_network(x):
    prediction = neural_network_model(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # The amount of cycles feed forward and with adjusting values (back propagation)
    hm_epochs = 10
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for epoch in range(hm_epochs):
            epoch_loss = 0
            i = 0
            while i < len(train_set):
                start = i
                end = i + batch_size
                batch_x = np.array(train_set[start:end])
                batch_y = np.array(train_labels[start:end])
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                epoch_loss += c
                i += batch_size
                print("{} images processed of epoch {}".format(i, epoch + 1))

            print('Epoch', epoch + 1, 'completed out of', hm_epochs, 'loss:', epoch_loss)
        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

        print('Accuracy:', accuracy.eval({x: test_set, y: test_labels}))


train_neural_network(x)
