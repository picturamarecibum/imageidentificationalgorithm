import sys
import tensorflow as tf

# Image dimensions
x = tf.placeholder("float", [None, 428,428,1])
y = tf.placeholder("float")


def CNN_network(input_data):
	"""
	Convolutional neural network with 3 convolutional layers and 3 pooling layers, not identical to Tflearn network.
	"""
	input_layer = input_data

	conv1 = tf.layers.conv2d(
		inputs=input_layer,
		filters=32,
		kernel_size=[2, 2],
		padding="same",
		activation=tf.nn.relu)

	pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

	# Convolutional Layer #2 and Pooling Layer #2
	conv2 = tf.layers.conv2d(
		inputs=pool1,
		filters=48,
		kernel_size=[2, 2],
		padding="same",
		activation=tf.nn.relu)

	pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

	# Convolutional Layer #3 and Pooling Layer #3
	conv3 = tf.layers.conv2d(
		inputs=pool2,
		filters=64,
		kernel_size=[2, 2],
		padding="same",
		activation=tf.nn.relu)

	pool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=[2, 2], strides=2)

	# Dense Layer
	pool3_flat = tf.reshape(pool3, [-1, 53 * 53 * 64])
	dense = tf.layers.dense(inputs=pool3_flat, units=1024, activation=tf.nn.relu)
	dropout = tf.layers.dropout(inputs=dense, rate=0.4, training=True)

	# Logits Layer
	logits = tf.layers.dense(inputs=dropout, units=5)
	return logits




def main(argv=None):
	if argv is None:
		argv = sys.argv


if __name__ == "__main__":
	sys.exit(main())
