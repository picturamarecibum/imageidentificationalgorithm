"""
The same convolutional neural network as in conv_plankton_network.py.
However, the model was converted from tensorflow to tflearn resulting in cleaner
code.
"""

import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
import data_shuffle
import data_transform
import plankton_preprocess
import numpy as np


def prepare_data(location):
    """
    The train, valid and testing sets are obtained and reshaped into usable shapes.
    """
    # Get the train, valid and testing set and convert to 3d numpy arrays
    train_set, train_labels, valid_set, valid_labels, test_set, test_labels = data_shuffle.add_files_to_process(
        location)
    print("loaded data")

    train_set = np.array(train_set)
    valid_set = np.array(valid_set)
    test_set = np.array(test_set)
    train_set = np.reshape(train_set, (train_set.shape[0], train_set.shape[1] * train_set.shape[2]))
    valid_set = np.reshape(valid_set, (valid_set.shape[0], valid_set.shape[1] * valid_set.shape[2]))
    test_set = np.reshape(test_set, (test_set.shape[0], test_set.shape[1] * test_set.shape[2]))

    # Perform PCA to reduce amount of dimensions
    train_set, valid_set, test_set = data_transform.perform_PCA(train_set, valid_set, test_set)
    print("Finished PCA")

    # Reshape data to 2d arrays
    train_set = np.reshape(train_set, [-1, 32, 32, 1])
    valid_set = np.reshape(valid_set, [-1, 32, 32, 1])
    test_set = np.reshape(test_set, [-1, 32, 32, 1])

    return train_set, train_labels, valid_set, valid_labels, test_set, test_labels, len(test_labels[0])


def CNN_model(nclasses):
    """
    The model of the convolutional neural network with two convolution and pooling layers.
    :type nclasses: Amount of classes to train with
    """
    # Input layer
    convnet = input_data(shape=[None, 32, 32, 1], name="input")

    # Convolutional layer #1 & #2 and pooling layer #1
    convnet = conv_2d(convnet, 64, [3, 3], activation="relu")
    convnet = conv_2d(convnet, 32, [3, 3], activation="relu")
    convnet = max_pool_2d(convnet, 2)

    # Convolutional layer #3 and #4 and pooling layer #2
    convnet = conv_2d(convnet, 48, [3, 3], activation="relu")
    convnet = conv_2d(convnet, 16, [3, 3], activation="relu")
    convnet = max_pool_2d(convnet, 2)

    # Convolutional layer #5 and pooling layer #3
    convnet = conv_2d(convnet, 64, [3, 3], activation="relu")
    convnet = max_pool_2d(convnet, 2)

    # Fully connected layer
    convnet = fully_connected(convnet, 1024, activation="relu")
    convnet = dropout(convnet, 0.7)

    # Output layer
    convnet = fully_connected(convnet, nclasses, activation="softmax")
    convnet = regression(convnet, optimizer="adam", learning_rate=0.001, name="targets")

    return convnet

def train_neural_network(location):
    """
    The neural network is trained using the training set and validated, afterwards the model predicts
    the labels of the test set and then the accuracy is calculated.
    """
    train_set, train_labels, valid_set, valid_labels, test_set, test_labels, nclasses = prepare_data(location)
    convnet = CNN_model(nclasses)
    model = tflearn.DNN(convnet)

    model.fit({"input": train_set}, {"targets": train_labels},
              n_epoch=10, validation_set=({'input': valid_set}, {'targets': valid_labels}),
              snapshot_step=500, show_metric=True, run_id="plankton", batch_size=16)

    # Apply model on test data set and print the accuracy
    results = model.predict(test_set)
    results = (results == results.max(axis=1)[:, None]).astype(int)
    print("accuracy: ", sum(np.all(np.asarray(results) == test_labels, axis=1)) / len(test_labels))

    return 0


def main():
    train_neural_network("/commons/student/2017-2018/Thema11/Lin_Sylt/")


if __name__ == "__main__":
    main()
