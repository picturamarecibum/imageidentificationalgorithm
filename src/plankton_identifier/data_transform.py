"""
Contains functions used for transforming the data in one way or another.
E.g. using PCA to transform the dataset for dimensionality reduction.
"""

from sklearn import decomposition


def perform_PCA(train_dataset, valid_dataset, test_dataset, components=1024):
    """
    Performs PCA to reduce the amount of dimensions in both the training and
    testing dataset. First the PCA will be fitted using 10% of the train dataset,
    then both datasets will be transformed using the PCA, reducing the
    amount of dimensions.
    :param train_dataset: Dataset used for training the model
    :param valid_dataset: Dataset for the validation
    :param test_dataset: The dataset used for testing the model
    :param components: the amount of components to use
                        MUST BE LARGER THAN SIZE OF DATASET FOR FIT
    :return: post-PCA train_dataset, test_dataset
    """
    svd = decomposition.TruncatedSVD(components)

    # Fit with at least 10% of the training set
    if train_dataset.shape[0] / 10 <= components:
        svd.fit(train_dataset[:components+1, ])
    else:
        svd.fit(train_dataset[:int(len(train_dataset) / 10), ])

    train_dataset = svd.transform(train_dataset)
    valid_dataset = svd.transform(valid_dataset)
    test_dataset = svd.transform(test_dataset)

    return train_dataset, valid_dataset, test_dataset
