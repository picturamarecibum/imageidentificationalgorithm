"""
Receive the arrays containing all the images, and split the images with a
70/20/10 split into respective training, validation and test datasets.
Before splitting the data is shuffled to reduce overfitting.
"""
import numpy as np
import sys
import plankton_preprocess


def add_files_to_process(path_of_directories, workstation_mode):
	"""
	Shuffle the data and split them into train, validation and test dataset.
	:param path_of_directories: Directory containing images to process
	"""
	images_with_labels = plankton_preprocess.PlanktonPreProcess(path_of_directories,workstation_mode)
	images_with_labels = np.array(images_with_labels.all_normalized_images)

	testing_size = int(0.1 * len(images_with_labels))
	validation_size = int(0.3 * len(images_with_labels))
	np.random.shuffle(images_with_labels)

	print(images_with_labels.shape)
	# Split the set into train, validation and test dataset (70/20/10 split)
	train_x = list(images_with_labels[:, 0][validation_size:])
	train_y = list(images_with_labels[:, 1][validation_size:])
	# train_names = list(images_with_labels[:, 2][validation_size:])
	valid_x = list(images_with_labels[:, 0][testing_size: validation_size])
	valid_y = list(images_with_labels[:, 1][testing_size: validation_size])
	# valid_names = list(images_with_labels[:, 2][validation_size:])
	test_x = list(images_with_labels[:, 0][:testing_size])
	test_y = list(images_with_labels[:, 1][:testing_size])
	# test_names = list(images_with_labels[:, 2][validation_size:])

	return train_x, train_y , valid_x, valid_y, test_x, test_y


def main(argv=None):
	if sys.argv is None:
		sys.argv = argv

	add_files_to_process("/commons/student/2017-2018/Thema11/Lin_Sylt/")

	return 0

if __name__ == "__main__":
	sys.exit(main())
