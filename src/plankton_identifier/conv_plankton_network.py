"""
Convolutional neural network

This uses 5 classes of the plankton dataset and uses a CNN using two convolutional and pooling layers.
Max pooling is used to get the resulting layers.
"""

import tensorflow as tf
import os
import data_shuffle
import data_transform
import plankton_preprocess
import numpy as np
from sklearn import decomposition
import time


train_set, train_labels, valid_set, valid_labels, test_set, test_labels = data_shuffle.add_files_to_process(
    "/commons/student/2017-2018/Thema11/Lin_Sylt/")

train_set = np.array(train_set)
train_set = np.reshape(train_set, (train_set.shape[0], train_set.shape[1] * train_set.shape[2]))
valid_set = np.array(valid_set)
valid_set = np.reshape(valid_set, (valid_set.shape[0], valid_set.shape[1] * valid_set.shape[2]))
test_set = np.array(test_set)
test_set = np.reshape(test_set, (test_set.shape[0], test_set.shape[1] * test_set.shape[2]))

# Perform PCA to reduce amount of dimensions
train_set, valid_set, test_set = data_transform.perform_PCA(train_set, valid_set, test_set)
print("Finished PCA")

# plankton_preprocess.dump_plankton_pickle("test_label.pickle", test_labels)

n_classes = 5  # The amount of classes in the dataset
batch_size = 16  # Amount of samples that are propagated through the network

x = tf.placeholder('float', [None, 1024])
y = tf.placeholder('float')

def covnet(dataje):
    input_layer = tf.reshape(dataje, [-1, 32, 32, 1])

    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=32,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu)

    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=64,
        kernel_size=[5, 5],
        padding="same",
        activation=tf.nn.relu)

    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

    # Dense Layer
    pool2_flat = tf.reshape(pool2, [-1, 4 * 4 * 256])
    dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
    dropout = tf.layers.dropout(inputs=dense, rate=0.4, training=True)

    # Logits Layer
    logits = tf.layers.dense(inputs=dropout, units=5)
    return logits


def train_neural_network(kaas):
    prediction = covnet(kaas)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # The amount of cycles feed forward and with adjusting values (back propagation)
    hm_epochs = 10
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for epoch in range(hm_epochs):
            epoch_loss = 0
            i = 0
            while i < len(train_set):
                start = i
                end = i + batch_size
                batch_x = np.array(train_set[start:end])
                batch_y = np.array(train_labels[start:end])
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                epoch_loss += c
                i += batch_size
                print("{} images processed out of {} epoch {}".format(i, len(train_set), epoch + 1))

            print('Epoch', epoch + 1, 'completed out of', hm_epochs, 'loss:', epoch_loss)
        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        # test_labels = plankton_preprocess.read_plankton_pickle("test_label.pickle")
        # test_set = plankton_preprocess.read_plankton_pickle("test_set.pickle")

        print('Accuracy:', accuracy.eval({x: test_set, y: test_labels}))

start = time.time()
train_neural_network(x)
print("took", time.time() - start)
