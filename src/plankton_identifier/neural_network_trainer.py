import tensorflow as tf
import sys
import numpy as np
import workstation_plankton_network
import lzma
import data_shuffle


# Adjustable batch size, current limit to 256.
batch_size = 16
# Image dimensions with a single colour channel(black,white).
x = tf.placeholder("float", [None, 428,428,1])
y = tf.placeholder("float")



def train_neural_network(train_set,train_labels ,test_set,test_labels,placeholder):
	"""
	Neural network trainer that decompresses the data when the batch is requested.
	"""
	prediction = workstation_plankton_network.CNN_network(placeholder)
	# Cost function
	cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=y))
	optimizer = tf.train.AdamOptimizer().minimize(cost)

	# The amount of cycles feed forward and with adjusting values (back propagation)
	hm_epochs = 10
	# Intialize the session
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		for epoch in range(hm_epochs):
			epoch_loss = 0
			i = 0
			# Iterate over training set and generate batch.
			while i < len(train_set):
				start = i
				end = i + batch_size
				# Decompress the batch with LZMA and feed it into the neural network.
				batch_x = np.array([np.fromstring(lzma.decompress(image)).reshape(428,428,1) for image in train_set[start:end]])
				batch_y = np.array([np.fromstring(lzma.decompress(label)) for label in train_labels[start:end]])
				# Feed the batch.
				_, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
				epoch_loss += c.copy()
				i += batch_size
				# The uncompressed data is garbage collected and removed, this does not affect the compressed data.
				print("{} images processed out of {} epoch {} ".format(i,len(train_set), epoch + 1))

			print('Epoch', epoch + 1, 'completed out of', hm_epochs, 'loss:', epoch_loss)
		correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
		accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
		print('Accuracy:', accuracy.eval({x: np.array([np.fromstring(lzma.decompress(image)).reshape(428,428,1) for image in test_set]), y: np.array([np.fromstring(lzma.decompress(i)) for i in test_labels])}))


def main(argv=None):
	"""
	Starts the neural network, only a directory with the images has to be given, all image folder that represent a class have to end with a - sign.
	"""
	if argv is None:
		argv = sys.argv
	# Request shuffled data for training and testing, validation set is currently not used in this trainer.
	# Last argument enables workstation mode.
	train_set, train_labels, valid_set, valid_labels, test_set, test_labels = data_shuffle.add_files_to_process(
		sys.argv[1],True)

	train_neural_network(train_set,train_labels,test_set,test_labels,x)
if __name__ == "__main__":
	sys.exit(main())
