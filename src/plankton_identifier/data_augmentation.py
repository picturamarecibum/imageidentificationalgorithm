"""
Methods for augmenting the images, to artificially enlarge the dataset.
This is done by e.g. rotating and mirroring the images.
"""

import sys
import numpy as np
import skimage.io
import skimage.transform


def mirror_image_left_right(image):
    """
    Mirror image from left to right.
    :param image: The image in a numpy array.
    :return: A copy of the mirrored numpy array as an image.
    """
    return np.copy(np.fliplr(image))


def rotate_image(img):
    """
    Rotates image at 90 degree angle.
    :param img: Image in numpy array.
    :return: Gives a list of 3 rotated images (90,180 and 270 degrees)
    """
    rotation_degrees = [90, 180, 270]

    return [skimage.transform.rotate(img, rotation) for rotation in rotation_degrees]


def main(argv=None):
    if argv is None:
        argv = sys.argv

    return 0


if __name__ == "__main__":
    sys.exit(main())
