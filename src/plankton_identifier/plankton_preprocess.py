"""
pre-processing of images of plankton.
The amount of images are counted in the folder_location, so it is known which shape the 3d array should be.
A 3d array (amount_of_images, 428, 428) is then created and is filled with the grayscale data of the images.

"""

import numpy as np
import glob
import sys
import os
import time
import _pickle as pickle
import skimage.transform
import skimage.io
import data_augmentation
import multiprocessing as mp
import lzma


class PlanktonPreProcess:
	def __init__(self, folder, workstation=False):
		"""
		Object that prepares images for the neural network by pasting it on a canvas of zeroes and compress them.
		Workstation mode enables data compression to reduce the ram usage.
		:param folder: The folder in which the sub folders for each class are located.
		"""
		manager = mp.Manager()
		# Enables data compression
		self.workstation_mode = workstation
		self.all_normalized_images = manager.list()
		self.image_and_label_process_allocator(folder)


	def image_and_label_process_allocator(self, folder):
		"""
		Scans the directories for folders ending with a:- .
		One folder at a time is split among the amount of CPU cores.
		:param folder: The folder in which the subfolders for each class are located,
		the folder names must end with a - character.
		"""
		procs = []
		# Detects all classes.
		pattern = glob.glob(folder+"*-/")
		template = np.zeros(len(pattern))
		# Index for one hot encoding.
		counter = -1
		for item in pattern:
			counter += 1
			print("Started class {}".format(item))
			# Modify one hot encoding character.
			template[counter] = 1
			all_images = glob.glob(item + "*")
			number_of_images = len(all_images)
			# Set CPU core limit.
			maximum_cores = round(os.cpu_count()/2)
			step_size = round(number_of_images / maximum_cores)
			if step_size == 0:
				step_size = 1

			# In case a division of the amount of cpu_cores is uneven.
			total_fragment_all_images = step_size * maximum_cores
			for step in range(0, number_of_images, step_size):
				# The remainder is taken, also no other processes should work on this data.
				if step + step_size == total_fragment_all_images:
					p = mp.Process(target=self.create_array_with_images_and_labels,
								   args=(all_images[step:None], template))
					procs.append(p)
					p.start()
					break
				else:
					# Start processing segment of an image class.
					p = mp.Process(target=self.create_array_with_images_and_labels,
								   args=(all_images[step:step + step_size], template))
					procs.append(p)
					p.start()
			# Finish jobs.
			for proc in procs:
				proc.join()
			# Reset one hot encoding for next class.
			template[counter] = 0
		print("Finished pre-processing.")

	def create_array_with_images_and_labels(self, folder_fragment, image_class):
		"""
		Paste each image on a canvas of zeroes, compress it with zlib and at it to the list.
		:param folder_fragment: The selection of images that is processed by a single core in a list.
		:param image_class: The one hot encoding of the class.
		"""

		for image in folder_fragment:
			# Create an empty array to paste the image on
			wall = np.zeros((428, 428), dtype=np.float)

			# Read image in as array in grayscale
			img = skimage.io.imread(image, as_grey=True)
			img[img == 255] = 0

			# Calculate the middle point of the image to paste on
			x = (wall.shape[0] - img.shape[0]) // 2
			y = (wall.shape[1] - img.shape[1]) // 2
			wall[x:x + img.shape[0], y:y + img.shape[1]] = img

			# Normalize image values from 0-255 to between -0.5 and 0.5
			normalized = (wall - 255 / 2) / 255

			# Workstation mode disabled
			if self.workstation_mode == False:
				# Normal image
				self.all_normalized_images.append(
					[(normalized)
						, np.copy(image_class)])
				# Mirrored image
				self.all_normalized_images.append(
					[data_augmentation.mirror_image_left_right(normalized)
						, np.copy(image_class)])
				# Rotated mirror and normal image
				for rotated_image in data_augmentation.rotate_image(normalized):
					self.all_normalized_images.append(
						[rotated_image
							,(np.copy(image_class))])
					self.all_normalized_images.append(
						[data_augmentation.mirror_image_left_right(rotated_image)
							, np.copy(image_class)])
			# Workstation mode enabled.
			else:
				# Compressed normal image.
				self.all_normalized_images.append(
					[lzma.compress((normalized))
						,lzma.compress(np.copy(image_class))])
				# Compressed mirrored image.
				self.all_normalized_images.append(
					[lzma.compress(data_augmentation.mirror_image_left_right(normalized))
						,lzma.compress(np.copy(image_class))])
				
				
				# Rotated and normal image compressed.
				for rotated_image in data_augmentation.rotate_image(normalized):
					self.all_normalized_images.append(
						[lzma.compress(rotated_image)
							,lzma.compress(np.copy(image_class))])
					self.all_normalized_images.append(
						[lzma.compress(data_augmentation.mirror_image_left_right(rotated_image))
							,lzma.compress(np.copy(image_class))])
		return


	def dump_plankton_pickle(self, input_dump_file, array_of_labels_and_images):
		"""
		Possiblity to pickle a dataset to offload the RAM for later use.
		"""
		with open(input_dump_file, "wb") as plankton_pickle_dump_file:
			print("Dumping data in pickle file: {}".format(input_dump_file))
			pickle.dump(array_of_labels_and_images, plankton_pickle_dump_file)
		return

	def read_plankton_pickle(self, input_load_file):
		"""
		Load pickled image dataset.
		"""
		with open(input_load_file, "rb") as plankton_pickle_load_file:
			print("Loading pickle file: {}".format(input_load_file))
			pickle.load(plankton_pickle_load_file)
		return


def main(argv=None):
	if sys.argv is None:
		sys.argv = argv

	folder_location = "/commons/student/2017-2018/Thema11/Lin_Sylt/"

	start = time.time()

	# total_images = count_files(folder_location)
	PlanktonPreProcess(folder_location)
	# create_array_with_images_and_labels(folder_location)
	# dump_plankton_pickle("/commons/student/2017-2018/Thema11/Lin_Sylt/arrays.pickle",data)
	# read_plankton_pickle("/commons/student/2017-2018/Thema11/Lin_Sylt/arrays.pickle")
	print("took:  : {:.2f} seconds".format(time.time() - start))

	return 0


if __name__ == "__main__":
	sys.exit(main())
