import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot = True)

n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500
n_classes = 10
batch_size = 100

x = tf.placeholder('float', [None, 784])
y = tf.placeholder('float')

def neural_network_model(data):
    hidden_1_layer = {'weights':tf.Variable(tf.random_normal([784, n_nodes_hl1])),
                      'biases':tf.Variable(tf.random_normal([n_nodes_hl1]))}

    hidden_2_layer = {'weights':tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
                      'biases':tf.Variable(tf.random_normal([n_nodes_hl2]))}

    hidden_3_layer = {'weights':tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
                      'biases':tf.Variable(tf.random_normal([n_nodes_hl3]))}

    output_layer = {'weights':tf.Variable(tf.random_normal([n_nodes_hl3, n_classes])),
                    'biases':tf.Variable(tf.random_normal([n_classes]))}

    hl1 = tf.add(tf.matmul(data,hidden_1_layer["weights"]), hidden_1_layer["biases"])
    hl2 = tf.nn.relu(tf.add(tf.matmul(hl1,hidden_2_layer["weights"]), hidden_2_layer["biases"]))
    hl3 = tf.nn.relu(tf.add(tf.matmul(hl2,hidden_3_layer["weights"]), hidden_3_layer["biases"]))

    return tf.add(tf.matmul(hl3,output_layer["weights"]), output_layer["biases"])

def train_neural_network(x):
    prediction = neural_network_model(x)
    print(prediction)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction,labels=y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # The amount of cycles feed forward and with adjusting values (back propagation)
    hm_epoch = 10
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for epoch in range(hm_epoch):
            epoch_loss = 0
            # The _ can be ignored, it is just to make the steps trough the training set.
            for _ in range(int(mnist.train.num_examples/batch_size)):
                # It shuffels the dataset and assigns the correct labels and values for it.
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                # Execute the optimizer and the cost function with the data from epoch_x and epoch_y
                _, c = sess.run([optimizer, cost], feed_dict={x: epoch_x, y: epoch_y})
                # add the result of the cost function to calculate the loss
                epoch_loss += c

            print('Epoch', epoch, 'completed out of', hm_epoch, 'loss:', epoch_loss)

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))

        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print('Accuracy:', accuracy.eval({x: mnist.test.images, y: mnist.test.labels}))

train_neural_network(x)
