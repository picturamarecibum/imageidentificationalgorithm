# Image Identification Algorithm

This repository is used for the project of theme 11 and 12, which is a machine learning project involving the identification of sea plankton.  

The project was split into several parts, First a logistic regression model was built and applied on the MNIST and the notMNIST dataset. A neural network was then introduced and applied on the actual plankton dataset, after preprocessing of the dataset.  
In theme 12 the model was improved with the use of a convolutional neural network. The parameters of the network were then tweaked to improve the classification.


## Authors:
- Lin Chang l.z.chang@st.hanze.nl
- Sylt Schuurmans s.s.schuurmans@st.hanze.nl